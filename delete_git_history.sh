#!/bin/bash

git filter-branch --index-filter "git rm --ignore-unmatch iso/rhde-ztp.iso" --tag-name-filter 'cat' -- --all
git for-each-ref --format="%(refname)" refs/original/ | xargs -n 1 git update-ref -d
git reflog expire --expire=now --all
git gc --prune=now
